/**
 * @file msg_type.h
 * @author Thibaut Artis thibaut.artis@ackl.io
 *
 * libcoap light integration implementation.
 */

#ifndef LIBCOAP_OSCORE_NATIVE_MSG_TYPE_H
#define LIBCOAP_OSCORE_NATIVE_MSG_TYPE_H

#include <stdlib.h>
#include <sys/types.h>

#include "libcoap.h"
#include "pdu.h"
#include "option.h"

#define COAP_EXTENDED_1_BYTE 13
#define COAP_EXTENDED_2_BYTE 14

#define COAP_EXTENDED_OFFSET_1 13
#define COAP_EXTENDED_OFFSET_2 269

#define COAP_OSCORE_OPTION 9

#define OSCORE_MAX_PAYLOAD_SIZE 255

typedef struct
{
  uint8_t buf[OSCORE_MAX_PAYLOAD_SIZE];
  size_t avail;
} oscore_buf_t;

#define OSCORE_BUF_INIT {.buf = {0}, .avail = OSCORE_MAX_PAYLOAD_SIZE}

typedef struct
{
  coap_pdu_t *pkt;
  oscore_buf_t *oscbuf;
} oscore_msg_native_t;

typedef struct
{
  coap_opt_iterator_t pos;
} oscore_msg_native_optiter_t;

typedef ssize_t oscore_msgerr_native_t;

#endif // LIBCOAP_OSCORE_NATIVE_MSG_TYPE_H