/**
 * @file oscore_test.c
 * @author Thibaut Artis thibaut.artis@ackl.io
 *
 * libcoap light integration implementation.
 */

#include <stdlib.h>
#include <stdio.h>

#include "oscore_native/test.h"

#include "pdu.h"

#define MESSAGE_DEFAULT_SIZE 256

static bool is_allocated = false;
static coap_pdu_t *the_message;
static oscore_msg_native_t the_enhanced_message;

oscore_msg_native_t oscore_test_msg_create(void)
{
  if (is_allocated)
  {
    abort();
  }

  the_message = coap_pdu_init(COAP_MESSAGE_NON, 0, 0, MESSAGE_DEFAULT_SIZE);

  is_allocated = true;

  the_enhanced_message.pkt = the_message;

  return the_enhanced_message;
}

void oscore_test_msg_destroy(oscore_msg_native_t message)
{
  if (!is_allocated)
  {
    abort();
  }

  coap_pdu_clear(the_message, MESSAGE_DEFAULT_SIZE);

  is_allocated = false;

  (void)message;
}